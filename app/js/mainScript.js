var mainPL;
var tmpPL;

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("header").style.fontSize = "30px";
  } else {
    document.getElementById("header").style.fontSize = "60px";
  }
}

function loadProducts() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (this.readyState == 4 && this.status == 200) {
		mainPL = JSON.parse(this.responseText);
		tmpPL = JSON.parse(this.responseText);
		createPList(mainPL);		
    }
  };
  xhttp.open("GET", "../src/data/products.json?t=" + Math.random(), true);
  xhttp.send();
}

function createPList(obj) {
  var plistStr = '';
  
  try{
	  if(null !== obj) {
		for(elem in obj){
		  if(null !== obj[elem]) {
            var purl = (obj[elem].url)?obj[elem].url:'#';
			var prating = (obj[elem].rating)?obj[elem].rating:0;
			var pname = (obj[elem].name)?(obj[elem].name.charAt(0).toUpperCase() + obj[elem].name.slice(1)):'';
			var psize = (obj[elem].size)?obj[elem].size:'';
			var ppic = (obj[elem].picture)?obj[elem].picture:'';
			var psav = (obj[elem].savings)?('You save '+obj[elem].savings):'';
			var poprice = (obj[elem].oldPrice)?obj[elem].oldPrice:'';
			var pprice = (obj[elem].price)?obj[elem].price:'';
			var pfav = (obj[elem].isFav)?obj[elem].isFav:false;
			if(!obj[elem]._id){
			  obj[elem]._id=elem+''+Math.ceil(Math.random()*100000);
			}
			var pid = obj[elem]._id;
					  
			plistStr += '<li id="' + pid +'">' + 
			 '<i class="wlist fa '+ (pfav?"fa-heart":"fa-heart-o") +'" onclick="favToggle(\''+ pid +'\', this, \'fa-heart-o\', \'fa-heart\');"></i>'+
				'<a href="'+ purl +'" class="product-photo">' +
					'<img src="' + ppic +'" width=370 height=250 alt="image">'+				
				'</a>'+
				'<div class="pdetails"><h2><a href="'+ purl +'" title="'+ pname +'">'+ pname +'</a></h2>'+				
				'<div class="product-rating">'+
				psize +	
					'<div>'+
							'<span class="product-stars" style="width: 60px">';
							
				if(prating > 0){
				  for(var i=0;i<prating;i++){
				    plistStr += '<i class="fa fa-star"></i>'
				  }
				} 
				
			plistStr += '</span>'+
					'</div>'+				
				'</div>'+
				'<div class="row"><span class="product-price">'+ pprice +'</span>'+'<span class="product-old-price">'+ poprice +'</span>'+
				'<span class="product-savings">'+ psav +'</span></div></div>'			
			'</li>';  	
		  } 
		}
		document.getElementById("plist").innerHTML = plistStr;
	  } 
	  document.getElementById("numRes").innerHTML = (obj?obj.length:0)+' results';
	  if(plistStr === ''){
		document.getElementById("numRes").innerHTML = '0 results';
	    document.getElementById("plist").innerHTML = '<h2>No products to display</h2>';
	  }	  
  } catch(err) {
	document.getElementById("numRes").innerHTML = '0 results';
    document.getElementById("plist").innerHTML = '<h2>No products to display</h2>';
  }  
}

function filterList() {	
  tmpPL = {};
  var prmin = parseFloat(document.getElementById('pmin').value);
  var prmax = parseFloat(document.getElementById('pmax').value);
  var selectIp = document.getElementById('sizeId');
  var siz = selectIp.options[selectIp.selectedIndex].text;
  console.log(prmin+"-"+prmax+"-"+siz+"-"+selectIp.selectedIndex);
  if(prmin > 0 && prmax > 0 && selectIp.selectedIndex == 0) {
	  tmpPL = mainPL.filter(element => (parseFloat(element.price.slice(1)) > prmin && parseFloat(element.price.slice(1)) < prmax));	
  } else if(prmin > 0 && prmax > 0 && document.getElementById('sizeId').selectedIndex > 0) {
	  tmpPL = mainPL.filter(element => (parseFloat(element.price.slice(1)) > prmin && parseFloat(element.price.slice(1)) < prmax && element.size === siz));	
  } else if(isNaN(prmin) && isNaN(prmax) && selectIp.selectedIndex > 0){
	  tmpPL = mainPL.filter( element => element.size === siz );
  } else if (isNaN(prmin) && isNaN(prmax) && selectIp.selectedIndex == 0){
	  tmpPL = mainPL;
  } 
  
  createPList(tmpPL);
  document.getElementById('sortId').selectedIndex = 0;
}

function clearFilters(){
	createPList(mainPL);	
	tmpPL = mainPL;
	document.getElementById('pmin').value = '';
	document.getElementById('pmax').value = '';
	document.getElementById('sizeId').selectedIndex = 0;
	document.getElementById('sortId').selectedIndex = 0;
}

// Sort function - 1. price (ascending), 2.price (descending), 3.name (ascending)
function sortPL(lst, sortby) {	
	if(sortby == 1){
	 tmpPL = lst.sort(function(a, b) {
	  return (parseFloat(a.price.slice(1)) - parseFloat(b.price.slice(1)))
	 });
	} else if(sortby == 2) {
	 tmpPL = lst.sort(function(a, b) {
	  return (parseFloat(b.price.slice(1)) - parseFloat(a.price.slice(1)))
	 });
	} else if(sortby == 3) {
	 tmpPL = lst.sort(function(a, b) {
	  //return (a.name.localeCompare(b.name)) // case insensitive
	  return ((a.name > b.name) - (a.name < b.name)) // case sensitive
	 });
	}
	
	if(sortby > 0 && tmpPL.length > 0) {
		createPList(tmpPL);	
		goTop();
	}
}

function goTop(){
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

// Wishlist toggle - favToggle(idx, this, 'fa-heart-o', 'fa-heart')
function favToggle(idx, elm, firstC, secondC){	
    var arrM = mainPL.find(elm => elm._id == idx);
	var arrT = tmpPL.find(elm => elm._id == idx);
	
	if (elm.classList.contains(firstC)){
     elm.classList.remove(firstC);
     elm.classList.add(secondC);
	 arrM.isFav = true;
	 arrT.isFav = true;
    } else {
     elm.classList.remove(secondC);
     elm.classList.add(firstC);
	 arrM.isFav = false;
	 arrT.isFav = false;
    }
}

// List/Grid View toggle  - listGridView(this, 1, 'plist', 'product-list-basic', 'product-list-vertical')
// 1. List View, 2. Grid View
function listGridView(btn, view, mainEl, gridC, listC) {
  elm = document.getElementById(mainEl);
  
  if(view == 1){
    if (elm.classList.contains(gridC)){
	  elm.classList.remove(gridC);
	  elm.classList.add(listC);
    } 
  } else if(view == 2){
    if (elm.classList.contains(listC)){
	  elm.classList.remove(listC);
	  elm.classList.add(gridC);
    }
  }
  setButtonActive(btn);  
}

function setButtonActive(btn){
  // Get the container element
  var btnContainer = document.getElementById("btnDiv");

  // Get all buttons with class="btn" inside the container
  var btns = btnContainer.getElementsByClassName("btn");

  // Remove active class from other button and add the active class to the current/clicked button
  for (var i = 0; i < btns.length; i++) {
    btns[i].classList.remove('active');  
  }
  btn.className += " active";
}